# frozen_string_literal: true

module Types
  module Base
    class Object < GraphQL::Schema::Object
      field_class Types::Base::Field

      def current_user
        context[:current_user]
      end
    end
  end
end
