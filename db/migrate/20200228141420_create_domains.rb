class CreateDomains < ActiveRecord::Migration[6.0]
  def change
    create_table :domains, id: :uuid do |t|
      t.string :name, null: false
      t.references :user, type: :uuid, null: false
      t.string :token, limit: 128, null: false
      t.string :slug, null: false

      t.timestamps
    end

    add_index :domains, %i(user_id name), unique: true
    add_index :domains, :slug, unique: true
  end
end
