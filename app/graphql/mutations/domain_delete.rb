module Mutations
  class DomainDelete < BaseMutation
     field :domain, Types::DomainType, null: false

     argument :id, ID, required: true

     def resolve id:
      domain = current_user.domains.find id
      {
        domain: domain.destroy
      }
      rescue ActiveRecord::RecordNotFound
        raise GraphQL::ExecutionError, "domain #{id} not found"
     end
  end
end
