module Types
  class SystemStatusType < Types::Base::Enum
    value "UNKNOWN", "System in unknown state. Has not contacted the API for some time", value: 'unknown'
    value "PENDING", "Confirmation pending from the system agent", value: 'pending'
    value "ACTIVE", "System is healthy", value: 'active'
    value "WARNING", "System is unhealthy", value: 'warning'
    value "OFFLINE", "System is offline", value: 'offline'
  end
end