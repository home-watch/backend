# frozen_string_literal: true

class ApplicationController < ActionController::API
  class UnauthorizedError < StandardError; end
  class NotFoundError < StandardError; end
  include Clearance::Controller

  rescue_from UnauthorizedError do |error|
    logger.error "Unauthorized: #{error}"
    render status: 401
  end

  rescue_from NotFoundError do |error|
    logger.error "NotFound: #{error}"
    render status: 404
  end

  protected

  def current_domain
    @current_domain || begin
      return unless api_token
      Domain.find_by_token api_token
    end
  end

  def require_current_domain
    raise UnauthorizedError, 'domain token required' unless current_domain.present?
  end

  def authenticate_via_token
    # return unless api_token
    user = User.first # TODO: implement token auth
    sign_in user if user
  end

  private

  def api_token
    pattern = /^Bearer /
    header  = request.env["HTTP_AUTHORIZATION"]
    header.gsub(pattern, '') if header && header.match(pattern)
  end
end
