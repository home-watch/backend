module Types
  class SystemType < Types::Base::Object
    field :id, ID, null: false
    field :name, String, null: false
    field :slug, String, null: false
    field :domain, Types::DomainType, null: false
    field :status, Types::SystemStatusType, null: false
    field :ipv4, String, null: true
    field :ipv6, String, null: true
    field :operating_system, String, null: true
    field :last_seen_at, GraphQL::Types::ISO8601DateTime, null: true
  end
end