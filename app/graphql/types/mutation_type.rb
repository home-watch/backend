# frozen_string_literal: true

module Types
  class MutationType < Types::Base::Object
    field :domain_create, mutation: Mutations::DomainCreate
    field :domain_delete, mutation: Mutations::DomainDelete

    field :system_remove, mutation: Mutations::SystemRemove
  end
end
