# == Schema Information
#
# Table name: systems
#
#  id               :uuid             not null, primary key
#  ipv4             :string
#  ipv6             :string
#  last_seen_at     :datetime
#  name             :string           not null
#  operating_system :string
#  slug             :string           not null
#  status           :integer          default("0"), not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  domain_id        :uuid             not null
#
# Indexes
#
#  index_systems_on_domain_id           (domain_id)
#  index_systems_on_domain_id_and_name  (domain_id,name) UNIQUE
#  index_systems_on_slug                (slug) UNIQUE
#
require 'rails_helper'

RSpec.describe System, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
