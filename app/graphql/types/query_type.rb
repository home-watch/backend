# frozen_string_literal: true

module Types
  class QueryType < Types::Base::Object
    field :ping, String, null: false
    field :current_user, Types::CurrentUserType, null: true

    def ping
      'pong'
    end
  end
end
