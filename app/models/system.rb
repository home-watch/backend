# == Schema Information
#
# Table name: systems
#
#  id               :uuid             not null, primary key
#  ipv4             :string
#  ipv6             :string
#  last_seen_at     :datetime
#  name             :string           not null
#  operating_system :string
#  slug             :string           not null
#  status           :integer          default("0"), not null
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  domain_id        :uuid             not null
#
# Indexes
#
#  index_systems_on_domain_id           (domain_id)
#  index_systems_on_domain_id_and_name  (domain_id,name) UNIQUE
#  index_systems_on_slug                (slug) UNIQUE
#
class System < ApplicationRecord
  extend FriendlyId
  friendly_id :slug_candidates
  
  belongs_to :domain
  enum status: %i(unknown pending active warning offline)

  validates :name, presence: true, uniqueness: { scope: :domain_id }

  def domain_name
    self.domain.name
  end

  def slug_candidates
    [
      [:domain_name, :name]
    ]
  end

  def should_generate_new_friendly_id?
    slug.blank? || domain.name_changed? || name_changed?
  end
end
