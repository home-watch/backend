module Mutations
  class SystemRemove < BaseMutation
     field :system, Types::SystemType, null: false

     argument :domain_id, ID, required: true
     argument :id, ID, required: true

     def resolve domain_id:, id:
      domain = begin
       current_user.domains.find domain_id
      rescue ActiveRecord::RecordNotFound
        raise GraphQL::ExecutionError, "domain #{id} not found"
      end

      system = domain.systems.find id
      raise GraphQL::ExecutionError, 'cannot delete ACTIVE system' if system.active?
      {
        system: system.destroy
      }
      rescue ActiveRecord::RecordNotFound
        raise GraphQL::ExecutionError, "system #{id} not found"
     end
  end
end
