module Types
  class CurrentUserType < Types::Base::Object
    field :id, ID, null: false
    field :username, String, null: false
    field :domains, Types::DomainType.connection_type, null: false
    field :domain, Types::DomainType, null: true do
      argument :id, ID, 'Domain ID or slug', required: true
    end

    def domain id:
      current_user.domains.find id
    rescue ActiveRecord::RecordNotFound
      raise GraphQL::ExecutionError, "no domain found for id \"#{id}\""
    end
  end
end