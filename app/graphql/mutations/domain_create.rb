module Mutations
  class DomainCreate < BaseMutation
     field :domain, Types::DomainType, null: false

     argument :name, String, required: true

     def resolve name:
       domain = current_user.domains.create name: name
       raise GraphQL::ExecutionError, domain.errors.full_messages.to_sentence unless domain.valid?
       {
           domain: domain
       }
     end
  end
end
