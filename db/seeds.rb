# frozen_string_literal: true
admin = User.find_or_create_by! email: 'admin@example.com', username: 'admin' do |user|
  user.password = 'admin'
end

home = admin.domains.find_or_create_by! name: 'Home'
# home.systems.find_or_create_by! name: 'Desktop'