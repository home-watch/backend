# == Schema Information
#
# Table name: domains
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  slug       :string           not null
#  token      :string(128)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :uuid             not null
#
# Indexes
#
#  index_domains_on_slug              (slug) UNIQUE
#  index_domains_on_user_id           (user_id)
#  index_domains_on_user_id_and_name  (user_id,name) UNIQUE
#
require 'rails_helper'

RSpec.describe Domain, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
