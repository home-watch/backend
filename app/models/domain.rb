# == Schema Information
#
# Table name: domains
#
#  id         :uuid             not null, primary key
#  name       :string           not null
#  slug       :string           not null
#  token      :string(128)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :uuid             not null
#
# Indexes
#
#  index_domains_on_slug              (slug) UNIQUE
#  index_domains_on_user_id           (user_id)
#  index_domains_on_user_id_and_name  (user_id,name) UNIQUE
#
class Domain < ApplicationRecord
  extend FriendlyId
  friendly_id :slug_candidates

  belongs_to :user
  has_many :systems

  before_create :generate_token

  validates :name, presence: true, uniqueness: { scope: :user_id }

  def username
    user.username
  end

  def reset_token!
    generate_token
    save validate: false
  end

  def slug_candidates
    [
      [:username, :name]
    ]
  end

  def should_generate_new_friendly_id?
    slug.blank? || user.username_changed? || name_changed?
  end

  private

  def generate_token
    self.token = Clearance::Token.new
  end
end
