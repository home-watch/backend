module Types
  class DomainType < Types::Base::Object
    field :id, ID, null: false
    field :name, String, null: false
    field :slug, String, null: false
    field :token, String, null: false
    field :systems, Types::SystemType.connection_type, null: false
    field :total_systems, Int, null: false
    field :active_systems, Int, null: false
    field :offline_systems, Int, null: false

    def total_systems
      object.systems.count
    end

    def active_systems
      object.systems.active.count
    end

    def offline_systems
      object.systems.offline.count
    end
  end
end