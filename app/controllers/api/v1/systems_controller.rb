module Api
  module V1
    class SystemsController < ApplicationController
      before_action :require_current_domain

      def enroll
        system = current_domain.systems.find_or_initialize_by name: system_params[:name] do |system|
          system.update_attributes system_params
          system.status = "pending"
        end

        if system.valid? && system.save
          render json: system, status: 201
        else
          logger.error system.errors.full_messages
          render json: system.errors, status: 400
        end
      end

      def ping
        system = current_domain.systems.find params[:id]
        raise NotFoundError, "system not found by id #{params[:id]}" unless system.present?

        system.last_seen_at = DateTime.current()
        unless params[:system].empty? || system.update(system_params)
          logger.error system.errors.full_messages
          return render json: system.errors, status: 400
        end

        system.active!
        render json: system
      end

      def offline
        system = current_domain.systems.find params[:id]
        raise NotFoundError, "system not found by id #{params[:id]}" unless system.present?

        system.last_seen_at = DateTime.current()
        system.offline!
        render json: system
      end

      private

      def system_params
        params.require(:system).permit(:name, :operating_system, :ipv4, :ipv6)
      end
    end
  end
end
