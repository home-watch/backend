# frozen_string_literal: true

Rails.application.routes.draw do
  post '/graphql', to: 'graphql#execute'

  namespace :api do
    namespace :v1 do
      post :systems, to: 'systems#enroll'
      put 'systems/:id', to: 'systems#ping'
      post 'systems/:id/offline', to: 'systems#offline'
    end
  end
end
