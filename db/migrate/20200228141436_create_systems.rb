class CreateSystems < ActiveRecord::Migration[6.0]
  def change
    create_table :systems, id: :uuid do |t|
      t.string :name, null: false
      t.references :domain, type: :uuid, null: false
      t.integer :status, null: false, default: 0
      t.string :slug, null: false
      t.string :operating_system, null: true
      t.string :ipv4, null: true
      t.string :ipv6, null: true
      t.datetime :last_seen_at, null: true

      t.timestamps
    end

    add_index :systems, %i(domain_id name), unique: true
    add_index :systems, :slug, unique: true
  end
end
